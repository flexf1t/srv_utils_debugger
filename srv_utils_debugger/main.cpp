#include "srv_utils_debugger.h"
#include <QtGui/QApplication>
#include <qtranslator.h>
#include <QTextCodec>

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);

	QTextCodec::setCodecForCStrings(QTextCodec::codecForLocale());
	
	srv_utils_debugger w;
	w.show();
	return a.exec();
}
