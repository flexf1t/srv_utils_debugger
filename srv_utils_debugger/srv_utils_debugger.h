#ifndef SRV_UTILS_DEBUGGER_H
#define SRV_UTILS_DEBUGGER_H

#include <QtGui/QMainWindow>
#include "ui_srv_utils_debugger.h"

struct DIM_HEAD;
struct RSWPassport;

class srv_utils_debugger : public QMainWindow
{
	Q_OBJECT

public:
	srv_utils_debugger(QWidget *parent = 0, Qt::WFlags flags = 0);
	~srv_utils_debugger();

private:
	Ui::srv_utils_debuggerClass ui;
	void setupTransform();

	QStringList projList;
	QStringList ellipsList;
	QListWidgetItem* clicked;
	QPixmap img;

	void* rmHandle;

	//pp
	void pp_showDimHead(DIM_HEAD *dh);
	//void pp_showRSWBasePassport(RSWPassport *passport);
private slots:
	//pm
	void pm_test();
	void pm_setFileName();
	void pm_save();
	//utils
	void u_transform_test();
	void u_transformProj4_test();
	//rm
	void rm_test();
	void rm_setResultFileName();
	void rm_setClsFileName();
	void rm_zoneToggled(bool checked);
	void rm_addList();
	void rm_removeList();
	void rm_clickedList(QListWidgetItem* item);
	//pp
	void pp_base_test();
	void pp_gdal_test();
	void pp_s57_gdal_test();
	void pp_tiff_test();
	void pp_rsw_mtw_test();
	void pp_setFileName();
};

#endif // SRV_UTILS_DEBUGGER_H
