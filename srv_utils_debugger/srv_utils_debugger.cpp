﻿#include "srv_utils_debugger.h"

#include <qfiledialog.h>
#include <qpixmap.h>

#include <previewmaker.h>
#include <srv_utils.h>
#include <regionmaker.h>

#include <cnv_base.h>
#include <gdal2dim.h>
#include <s572dim.h>
#include <dgt_img/tif/passport.h>
#include <dgt_img/rsw/passport.h>

#include <map_wrk.h>

#include <dgt_base.h>

srv_utils_debugger::srv_utils_debugger(QWidget *parent, Qt::WFlags flags)
	: QMainWindow(parent, flags)
{
	ui.setupUi(this);
	setupTransform();
	//setup previewmaker
	connect(ui.pm_pathButton, SIGNAL(clicked()), this, SLOT(pm_setFileName()));
	connect(ui.pm_testButton, SIGNAL(clicked()), this, SLOT(pm_test()));
	connect(ui.pm_save, SIGNAL(clicked()), this, SLOT(pm_save()));
	//setup regionmaker
	clicked = NULL;
	rmHandle = NULL;
	connect(ui.rm_clsPathButton, SIGNAL(clicked()), this, SLOT(rm_setClsFileName()));
	connect(ui.rm_resultPathButton, SIGNAL(clicked()), this, SLOT(rm_setResultFileName()));
	connect(ui.rm_zone, SIGNAL(toggled(bool)), this, SLOT(rm_zoneToggled(bool)));
	connect(ui.rm_addListButton, SIGNAL(clicked()), this, SLOT(rm_addList()));
	connect(ui.rm_removeListButton, SIGNAL(clicked()), this, SLOT(rm_removeList()));
	connect(ui.rm_mapLists, SIGNAL(itemPressed(QListWidgetItem*)), this, SLOT(rm_clickedList(QListWidgetItem*)));
	connect(ui.rm_testButton, SIGNAL(clicked()), this, SLOT(rm_test()));
	//setup passports
	connect(ui.pp_pathButton, SIGNAL(clicked()), this, SLOT(pp_setFileName()));
	connect(ui.pp_base, SIGNAL(clicked()), this, SLOT(pp_base_test()));
	connect(ui.pp_gdal, SIGNAL(clicked()), this, SLOT(pp_gdal_test()));
	connect(ui.pp_s57_gdal, SIGNAL(clicked()), this, SLOT(pp_s57_gdal_test()));
	connect(ui.pp_tiff, SIGNAL(clicked()), this, SLOT(pp_tiff_test()));
	connect(ui.pp_rsw_mtw, SIGNAL(clicked()), this, SLOT(pp_rsw_mtw_test()));
	
}

srv_utils_debugger::~srv_utils_debugger()
{

}

void srv_utils_debugger::setupTransform()
{
	projList.clear();
	projList << "ipUNDEFINED = 0";
	projList << "ipGAUSSCONFORMAL = 1";
	projList << "ipCONICALORTHOMORPHIC = 2";
	projList << "ipCYLINDRICALSPECIAL = 3";
	projList << "ipLAMBERT = 4";
	projList << "ipSTEREOGRAPHIC = 5";
	projList << "ipPOSTEL = 6";
	projList << "ipAZIMUTHALOBLIQUE = 7";
	projList << "ipMERCATORMAP = 8";
	projList << "ipURMAEV = 9";
	projList << "ipPOLYCONICAL = 10";
	projList << "ipSIMPLEPOLYCONICAL = 11";
	projList << "ipPSEUDOCONICAL = 12";
	projList << "ipSTEREOGRAPHICPOLAR = 13";
	projList << "ipCHEBISHEV = 14";
	projList << "ipGNOMONIC = 15";
	projList << "ipCYLINDRICALSPECIALBLANK = 16";
	projList << "ipUTM = 17";
	projList << "ipKAVRAJSKY = 18";
	projList << "ipMOLLWEIDE = 19";
	projList << "ipCONICALEQUIDISTANT = 20";
	projList << "ipCONICALEQUALAREA = 21";
	projList << "ipCONICALDIRECTORTHOMORPHIC = 22";
	projList << "ipAZIMUTHALORTHOMORPHICPOLAR = 23";
	projList << "ipLAMBERTAZIMUTHALEQUALAREA = 24";
	projList << "ipURMAEVSINUSOIDAL = 25";
	projList << "ipAITOFF = 26";
	projList << "ipCYLINDRICALEQUALSPACED = 27";
	projList << "ipLAMBERTCYLINDRICALEQUALAREA = 28";
	projList << "ipMODIFIEDPOLYCONICAL = 29";
	projList << "ipLAMBERTOBLIQUEAZIMUTHAL = 30";
	projList << "ipTRANSVERSECYLINDRICAL = 31";
	projList << "ipGAUSSCONFORMAL_SYSTEM_63 = 32";
	projList << "ipKAVRAJSKY_CONIC_EQUIDISTANT = 33";
	
	ui.u_transformProj->addItems(projList);
	ui.u_transformProj->setCurrentIndex(1);

	ellipsList.clear();
	ellipsList << "ieUNDEFINED = 0";  // Не установлено
	ellipsList << "ieKRASOVSKY42 = 1";  // Красовского 1942г.
	ellipsList << "ieWGS_72 = 2";  // Международный 1972г.
	ellipsList << "ieHEFORD = 3";  // Хейфорда 1909г.
	ellipsList << "ieCLARKE_80 = 4";  // Кларка 1880г.
	ellipsList << "ieCLARKE_66 = 5";  // Кларка 1866г.
	ellipsList << "ieEVEREST_57 = 6";  // Эвереста 1857г.
	ellipsList << "ieBESSEL = 7";  // БесселЯ 1841г.
	ellipsList << "ieAIRY = 8";  // Эри 1830г.
	ellipsList << "ieWGS_84 = 9";  // Международный 1984г.
	ellipsList << "iePZ_90 = 10";  // ПЗ-90.02
	ellipsList << "ieGSK_11 = 46";  // Для геодезической СК 2011
	ellipsList << "iePZ_9011 = 47";  // ПЗ-90.11 != ПЗ-90.02
	ui.u_transformEllips->addItems(ellipsList);
	ui.u_transformEllips->setCurrentIndex(1);
	
	connect(ui.u_transform_test, SIGNAL(clicked()), this, SLOT(u_transform_test()));
	connect(ui.u_transformProj4_test, SIGNAL(clicked()), this, SLOT(u_transformProj4_test()));
}

void srv_utils_debugger::u_transform_test()
{
	int proj = ui.u_transformProj->currentIndex();
	int ellipsoid = ui.u_transformEllips->currentIndex();
	if (ellipsoid == 11 || ellipsoid == 12)
		ellipsoid += 35;
	double b0 = ui.u_transformB0->text().toDouble();
	double b1 = ui.u_transformB1->text().toDouble();
	double l0 = ui.u_transformL0->text().toDouble();
	bool north = ui.u_transformNorth->currentIndex(); // 0 - "Нет" = false, 1 - "Да" = true
	double x = ui.u_transformX->text().toDouble();
	double y = ui.u_transformY->text().toDouble();
	double resultX = 0;
	double resultY = 0;

	if (ui.u_transformSelect->currentIndex() == 0) // 0 - "метры в градусы", 1 - "градусы в метры"
		TransformXY_to_BL(proj, ellipsoid, b0, b1, l0, north, x, y, &resultX, &resultY);
	else
		TransformBL_to_XY(proj, ellipsoid, b0, b1, l0, north, x, y, &resultX, &resultY);

	ui.u_resultX->setText(QString::number(resultX, 'g', 12));
	ui.u_resultY->setText(QString::number(resultY, 'g', 12));

	return;
}

void srv_utils_debugger::u_transformProj4_test()
{
	QString source = ui.u_transformProj4Source->text();
	QString dest = ui.u_transformProj4Dest->text();
	double x = ui.u_transformProj4X->text().toDouble();
	double y = ui.u_transformProj4Y->text().toDouble();
	double resultX = 0;
	double resultY = 0;
	QByteArray sba = source.toAscii();
	QByteArray dba = dest.toAscii();
	char *s = sba.data();
	char *d = dba.data();
	bool result = TransformProj(s, d, x, y, &resultX, &resultY);
	if (result)
	{
		ui.u_resultX->setText(QString::number(resultX, 'g', 12));
		ui.u_resultY->setText(QString::number(resultY, 'g', 12));
	}
	else
	{
		ui.u_resultX->setText("error");
		ui.u_resultY->setText("error");
	}

	return;
}

void srv_utils_debugger::pm_setFileName()
{
	QString filePath = QFileDialog::getOpenFileName(this, "Открыть", ui.pm_path->text(), "");
	ui.pm_path->setText(filePath);
}

void srv_utils_debugger::pm_test()
{
	QString filePath = ui.pm_path->text();
	int size = ui.pm_sizeSpin->value();
	char *image = new char[size * size * 3];
	ui.pm_save->setEnabled(false);
	int result = CreatePreview(filePath.toLocal8Bit().data(), image, size, 0, 0, ui.pm_qualitySpin->value());
	if (result > 0)
	{
		img.loadFromData(reinterpret_cast<const uchar*>(image), result);
		//img.save("D:/maps/preview.png");
		ui.pm_img->setPixmap(img);
		ui.pm_output->setText("Ок! размер : " + QString::number(result) + " байт.");
		ui.pm_save->setEnabled(true);
	}
	else
	{
		ui.pm_output->setText("Чтото пошло не так! Код ошибки: " + QString::number(result) + " \nappPath: " + QCoreApplication::applicationDirPath());
	}
	delete [] image;
	return;
}

void srv_utils_debugger::pm_save()
{
	if (img.isNull())
		return;
	QString filePath = QFileDialog::getSaveFileName(this, "Сохранить как", ui.pm_path->text(), "*.png");
	img.save(filePath);
}


void srv_utils_debugger::rm_test()
{
	int proj = ((ui.rm_proj->currentIndex() == 1) ? 1 : 8);
	int axis = -1; //auto
	if (ui.rm_center->isChecked())
		axis = 0;
	else if (ui.rm_zone->isChecked())
		axis = ui.rm_zoneNumber->value();
	QByteArray cls = ui.rm_clsPath->text().toAscii();
	QByteArray path = ui.rm_resultPath->text().toAscii();

	int count = ui.rm_mapLists->count();

	if (path.isEmpty() || count == 0)
		return;

	char **files = new char*[count];
	for (int i = 0; i < count; i++)
	{
		QString str = ui.rm_mapLists->item(i)->text();
		files[i] = new char[str.size() + 1];
		QByteArray ba = str.toAscii();
		strcpy(files[i], ba.data());
	}

	ui.rm_progress->setValue(0);
	ui.rm_progress->setMaximum(count);
	ui.rm_testButton->setEnabled(false);

	rmHandle = InitRegionMaker();
	int result = CreateRegionFile(rmHandle, files, count, cls.data(), path.data(), proj, axis);
	ui.rm_progress->setValue(result);
	ui.rm_testButton->setEnabled(true);

	for (int i = 0; i < count; i++)
	{ 
		delete [] files[i];
	}
	delete [] files;
	DeleteRegionMaker(rmHandle);
	rmHandle = NULL;
}

void srv_utils_debugger::rm_setResultFileName()
{
	QString filePath = QFileDialog::getSaveFileName(this, "Сохранить", ui.rm_resultPath->text(), "");
	ui.rm_resultPath->setText(filePath);
}

void srv_utils_debugger::rm_setClsFileName()
{
	QString filePath = QFileDialog::getOpenFileName(this, "Открыть", ui.rm_clsPath->text(), "");
	ui.rm_clsPath->setText(filePath);
}

void srv_utils_debugger::rm_zoneToggled(bool checked)
{
	ui.rm_zoneNumber->setEnabled(checked);
}

void srv_utils_debugger::rm_addList()
{
	int count = ui.rm_mapLists->count();
	QString path = "";
	if (count != 0)
	{
		QListWidgetItem* item = ui.rm_mapLists->item(count - 1);
		path = item->text();
	}
	QString filePath = QFileDialog::getOpenFileName(this, "Добавить", path, "");
	if (ui.rm_mapLists->findItems(filePath, Qt::MatchFixedString).count() == 0)
	{
		ui.rm_mapLists->addItem(filePath);
		ui.rm_mapLists->item(count)->setCheckState(Qt::Checked);
		ui.rm_testButton->setEnabled(true);
	}
}

void srv_utils_debugger::rm_removeList()
{
	if (clicked)
	{
		delete clicked;
		clicked = NULL;
	}
	if (ui.rm_mapLists->count() == 0)
	{
		ui.rm_testButton->setEnabled(false);
		ui.rm_removeListButton->setEnabled(false);
	}
}

void srv_utils_debugger::rm_clickedList(QListWidgetItem* item)
{
	clicked = item;
	ui.rm_removeListButton->setEnabled(true);
}
//pp
void srv_utils_debugger::pp_setFileName()
{
	QString filePath = QFileDialog::getOpenFileName(this, "Открыть", ui.pp_filePath->text(), "");
	ui.pp_filePath->setText(filePath);
}

void srv_utils_debugger::pp_s57_gdal_test()
{
	S57Passport passport = {0};
	bool result = false;
	ui.pp_output->clear();	
	result = S57HeadersGetFromGDAL(ui.pp_filePath->text().toLocal8Bit().data(), &passport);
	
	if(result)
	{
		ui.pp_output->appendPlainText("upperLeftX: " + QString::number(passport.upperLeftX, 'f', 12));
		ui.pp_output->appendPlainText("upperLeftY: " + QString::number(passport.upperLeftY, 'f', 12));
		ui.pp_output->appendPlainText("lowerLeftX: " + QString::number(passport.lowerLeftX, 'f', 12));
		ui.pp_output->appendPlainText("lowerLeftY: " + QString::number(passport.lowerLeftY, 'f', 12));
		ui.pp_output->appendPlainText("upperRightX: " + QString::number(passport.upperRightX, 'f', 12));
		ui.pp_output->appendPlainText("upperRightY: " + QString::number(passport.upperRightY, 'f', 12));
		ui.pp_output->appendPlainText("lowerRightX: " + QString::number(passport.lowerRightX, 'f', 12));
		ui.pp_output->appendPlainText("lowerRightY: " + QString::number(passport.lowerRightY, 'f', 12) + "\n");
			
		ui.pp_output->appendPlainText("Тип координат. 1 - градусы: " + QString::number(passport.coordType));
		ui.pp_output->appendPlainText("Эллипсоид. всегда ieWGS_84: " + QString::number(passport.ellipsoid));
		ui.pp_output->appendPlainText("Дата выпуска: " + QString(passport.issueDate));
		ui.pp_output->appendPlainText("Дата обновления: " + QString(passport.updateDate));
		ui.pp_output->appendPlainText("Навигационное назначение: " + QString::number(passport.navigationalPurpose));
		ui.pp_output->appendPlainText("Номер издания: " + QString::number(passport.editionNumber));
		ui.pp_output->appendPlainText("Номер обновления: " + QString::number(passport.updateNumber));
		ui.pp_output->appendPlainText("Версия издательства S-57: " + QString(passport.version));
		ui.pp_output->appendPlainText("Количество объектов: " + QString::number(passport.geoRecordsCount));
		ui.pp_output->appendPlainText("Масштаб: " + QString::number(passport.scale));
		ui.pp_output->appendPlainText("Producing agency: " + QString::number(passport.producingAgency));
		ui.pp_output->appendPlainText("Номенклатура: " + QString(passport.dataSetName));
		ui.pp_output->appendPlainText("comment: " + QString(passport.comment));
	}
	else
		ui.pp_output->appendPlainText("Не удалось прочитать пасспорт");
	return;
}

void srv_utils_debugger::pp_tiff_test()
{
	bool result = false;
	ui.pp_output->clear();
	TIFPassport passport = {0};
	result = TIFHeadersGet(ui.pp_filePath->text().toLocal8Bit().data(), &passport);
	if (result == false)
	{
		ui.pp_output->appendPlainText("Не удалось прочитать пасспорт");
		return;
	}
	ui.pp_output->appendPlainText("Model: " + QString::number(passport.Model));
	ui.pp_output->appendPlainText("PCS: " + QString::number(passport.PCS));
	ui.pp_output->appendPlainText("GCS: " + QString::number(passport.GCS));
	ui.pp_output->appendPlainText("UOMLength: " + QString::number(passport.UOMLength));
	ui.pp_output->appendPlainText("UOMLengthInMeters: " + QString::number(passport.UOMLengthInMeters, 'f', 12));
	ui.pp_output->appendPlainText("UOMAngle: " + QString::number(passport.UOMAngle));
	ui.pp_output->appendPlainText("UOMAngleInDegrees: " + QString::number(passport.UOMAngleInDegrees, 'f', 12));
	ui.pp_output->appendPlainText("Datum: " + QString::number(passport.Datum));
	ui.pp_output->appendPlainText("PM: " + QString::number(passport.PM));
	ui.pp_output->appendPlainText("PMLongToGreenwich: " + QString::number(passport.PMLongToGreenwich, 'f', 12));
	ui.pp_output->appendPlainText("Ellipsoid: " + QString::number(passport.Ellipsoid));
	ui.pp_output->appendPlainText("SemiMajor: " + QString::number(passport.SemiMajor, 'f', 12));
	ui.pp_output->appendPlainText("SemiMinor: " + QString::number(passport.SemiMinor, 'f', 12));
	ui.pp_output->appendPlainText("ProjCode: " + QString::number(passport.ProjCode));
	ui.pp_output->appendPlainText("Projection: " + QString::number(passport.Projection));
	ui.pp_output->appendPlainText("CTProjection: " + QString::number(passport.CTProjection));
	ui.pp_output->appendPlainText("nParms: " + QString::number(passport.nParms));
	ui.pp_output->appendPlainText("MapSys: " + QString::number(passport.MapSys));
	ui.pp_output->appendPlainText("Zone: " + QString::number(passport.Zone));
	ui.pp_output->appendPlainText("DefnSet: " + QString::number(passport.DefnSet));
	ui.pp_output->appendPlainText("\nupperLeftX: " + QString::number(passport.upperLeftX, 'f', 12));
	ui.pp_output->appendPlainText("upperLeftY: " + QString::number(passport.upperLeftY, 'f', 12));
	ui.pp_output->appendPlainText("lowerLeftX: " + QString::number(passport.lowerLeftX, 'f', 12));
	ui.pp_output->appendPlainText("lowerLeftY: " + QString::number(passport.lowerLeftY, 'f', 12));
	ui.pp_output->appendPlainText("upperRightX: " + QString::number(passport.upperRightX, 'f', 12));
	ui.pp_output->appendPlainText("upperRightY: " + QString::number(passport.upperRightY, 'f', 12));
	ui.pp_output->appendPlainText("lowerRightX: " + QString::number(passport.lowerRightX, 'f', 12));
	ui.pp_output->appendPlainText("lowerRightY: " + QString::number(passport.lowerRightY, 'f', 12) + "\n");
	if (passport.str_datum != NULL) ui.pp_output->appendPlainText("str_datum: " + QString(passport.str_datum));
	if (passport.str_ellips != NULL) ui.pp_output->appendPlainText("str_ellips: " + QString(passport.str_ellips));
	if (passport.str_CS != NULL) ui.pp_output->appendPlainText("str_CS: " + QString(passport.str_CS));
	if (passport.str_Proj != NULL) ui.pp_output->appendPlainText("str_Proj: " + QString(passport.str_Proj));
	if (passport.str_PCS != NULL) ui.pp_output->appendPlainText("str_PCS: " + QString(passport.str_PCS)); 
}

void srv_utils_debugger::pp_base_test()
{
	QString str = QFileInfo(ui.pp_filePath->text()).suffix();//completeSuffix();
	Qt::CaseSensitivity cs = Qt::CaseInsensitive;
	DIM_HEAD dh = {0};
	bool result = false;
	ui.pp_output->clear();
	if (!str.compare("sxf", cs))
	{
		//V4_SXF_PASSPORT sp = {0};
		//bool ok = SXFHeadersGet(ui.pp_filePath->text().toLocal8Bit().data(), &sp);
		result = HeaderGetFromSXF(ui.pp_filePath->text().toLocal8Bit().data(), &dh);
	}
	else if (!str.compare("txf", cs)) result = HeaderGetFromTXF(ui.pp_filePath->text().toLocal8Bit().data(), &dh);
	else if (!str.compare("000", cs)) result = HeaderGetFromS57(ui.pp_filePath->text().toLocal8Bit().data(), &dh);
	else if (!str.compare("gxf", cs)) result = HeaderGetFromGXF(ui.pp_filePath->text().toLocal8Bit().data(), &dh);
	else if (!str.compare("mif", cs)) result = HeaderGetFromMIF(ui.pp_filePath->text().toLocal8Bit().data(), &dh);
	else if (!str.compare("shp", cs)) result = HeaderGetFromSHP(ui.pp_filePath->text().toLocal8Bit().data(), &dh);
	else 
	{
		ui.pp_output->appendPlainText("Паспорт для этого формата не поддерживается.");
		return;
	}
	if (result)
		pp_showDimHead(&dh);
	else
		ui.pp_output->appendPlainText("Не удалось прочитать пасспорт");
	return;
}

void srv_utils_debugger::pp_gdal_test()
{
	DIM_HEAD dh = {0};
	bool result = false;
	ui.pp_output->clear();	
	result = HeaderGetFromGDAL(ui.pp_filePath->text().toLocal8Bit().data(), &dh);
	
	if (result)
		pp_showDimHead(&dh);
	else
		ui.pp_output->appendPlainText("Не удалось прочитать пасспорт");
	return;
}

void srv_utils_debugger::pp_rsw_mtw_test()
{
	QString str = QFileInfo(ui.pp_filePath->text()).suffix();//completeSuffix();
	Qt::CaseSensitivity cs = Qt::CaseInsensitive;
	bool result = false;
	int type = 0; 
	RSWPassport passportR;
	MTWPassport passportM;

	ui.pp_output->clear();
	if (!str.compare("mtw",cs))
	{ 
		type = 1;// mtw
		result = MTWHeadersGet(ui.pp_filePath->text().toLocal8Bit().data(), &passportM);
		if (result)
		{
			ui.pp_output->appendPlainText("Тип координат. 0 - метры, 1 - градусы: " + QString::number(passportM.coordType));
			ui.pp_output->appendPlainText("\nupperLeftX: " + QString::number(passportM.upperLeftX, 'f', 12));
			ui.pp_output->appendPlainText("upperLeftY: " + QString::number(passportM.upperLeftY, 'f', 12));
			ui.pp_output->appendPlainText("lowerLeftX: " + QString::number(passportM.lowerLeftX, 'f', 12));
			ui.pp_output->appendPlainText("lowerLeftY: " + QString::number(passportM.lowerLeftY, 'f', 12));
			ui.pp_output->appendPlainText("upperRightX: " + QString::number(passportM.upperRightX, 'f', 12));
			ui.pp_output->appendPlainText("upperRightY: " + QString::number(passportM.upperRightY, 'f', 12));
			ui.pp_output->appendPlainText("lowerRightX: " + QString::number(passportM.lowerRightX, 'f', 12));
			ui.pp_output->appendPlainText("lowerRightY: " + QString::number(passportM.lowerRightY, 'f', 12) + "\n");
	
			ui.pp_output->appendPlainText("версия структуры: " + QString::number(passportM.version));
			ui.pp_output->appendPlainText("размер файла в байтах: " + QString::number(passportM.fileSize));
			ui.pp_output->appendPlainText("пользовательский идентификатор: " + QString::number(passportM.userIdent));
			ui.pp_output->appendPlainText("условное имя: " + QString(passportM.imgName));
			ui.pp_output->appendPlainText("высота изображения: " + QString::number(passportM.height));
			ui.pp_output->appendPlainText("ширина изображения: " + QString::number(passportM.width));
			ui.pp_output->appendPlainText("версия структуры: " + QString::number(passportM.mapType));
			ui.pp_output->appendPlainText("Тип карты: " + QString::number(passportM.projType));
			ui.pp_output->appendPlainText("Эллипсоид: " + QString::number(passportM.ellipsoid));
			ui.pp_output->appendPlainText("Масштаб: " + QString::number(passportM.scale));
			ui.pp_output->appendPlainText("Разрешение(точек на метр): " + QString::number(passportM.resol));
			ui.pp_output->appendPlainText("Количество метров местности на элемент: " + QString::number(passportM.meterPerElem));
			ui.pp_output->appendPlainText("Первая главная параллель  (В градусах): " + QString::number(passportM.mainLati1));
			ui.pp_output->appendPlainText("Вторая главная параллель: " + QString::number(passportM.mainLati2));
			ui.pp_output->appendPlainText("Осевой меридиан (В градусах): " + QString::number(passportM.mainLongi));
			ui.pp_output->appendPlainText("Параллель главной точки (В градусах): " + QString::number(passportM.latiMainPnt));
			ui.pp_output->appendPlainText("Минимальное значение высоты рельефа: " + QString::number(passportM.minReliefH));
			ui.pp_output->appendPlainText("Максимальное значение высоты рельефа: " + QString::number(passportM.maxReliefH));
			ui.pp_output->appendPlainText("Единица измерения по высоте: " + QString::number(passportM.reliefType));
			ui.pp_output->appendPlainText("Тип матрицы: " + QString::number(passportM.matrixType));
		}
	}
	else if (!str.compare("rsw",cs))
	{
		type = 2;// rsw
		result = RSWHeadersGet(ui.pp_filePath->text().toLocal8Bit().data(), &passportR);
		if (result)
		{
			ui.pp_output->appendPlainText("Тип координат. 0 - метры, 1 - градусы: " + QString::number(passportR.coordType));
			ui.pp_output->appendPlainText("\nupperLeftX: " + QString::number(passportR.upperLeftX, 'f', 12));
			ui.pp_output->appendPlainText("upperLeftY: " + QString::number(passportR.upperLeftY, 'f', 12));
			ui.pp_output->appendPlainText("lowerLeftX: " + QString::number(passportR.lowerLeftX, 'f', 12));
			ui.pp_output->appendPlainText("lowerLeftY: " + QString::number(passportR.lowerLeftY, 'f', 12));
			ui.pp_output->appendPlainText("upperRightX: " + QString::number(passportR.upperRightX, 'f', 12));
			ui.pp_output->appendPlainText("upperRightY: " + QString::number(passportR.upperRightY, 'f', 12));
			ui.pp_output->appendPlainText("lowerRightX: " + QString::number(passportR.lowerRightX, 'f', 12));
			ui.pp_output->appendPlainText("lowerRightY: " + QString::number(passportR.lowerRightY, 'f', 12) + "\n");
	
			ui.pp_output->appendPlainText("версия структуры: " + QString::number(passportR.version));
			ui.pp_output->appendPlainText("размер файла в байтах: " + QString::number(passportR.fileSize));
			ui.pp_output->appendPlainText("пользовательский идентификатор: " + QString::number(passportR.userIdent));
			ui.pp_output->appendPlainText("условное имя: " + QString(passportR.imgName));
			ui.pp_output->appendPlainText("размер элемента в битах. RSW:1,4,8,16,24,32: " + QString::number(passportR.bitsPerPixel));
			ui.pp_output->appendPlainText("высота изображения: " + QString::number(passportR.height));
			ui.pp_output->appendPlainText("ширина изображения: " + QString::number(passportR.width));
			ui.pp_output->appendPlainText("версия структуры: " + QString::number(passportR.mapType));
			ui.pp_output->appendPlainText("Тип карты: " + QString::number(passportR.projType));
			ui.pp_output->appendPlainText("Эллипсоид. всегда ieKRASOVSKY42: " + QString::number(passportR.ellipsoid));
			ui.pp_output->appendPlainText("Масштаб: " + QString::number(passportR.scale));
			ui.pp_output->appendPlainText("Разрешение(точек на метр): " + QString::number(passportR.resol));
			ui.pp_output->appendPlainText("Количество метров местности на элемент: " + QString::number(passportR.meterPerElem));
			ui.pp_output->appendPlainText("Первая главная параллель  (В градусах): " + QString::number(passportR.mainLati1));
			ui.pp_output->appendPlainText("Вторая главная параллель: " + QString::number(passportR.mainLati2));
			ui.pp_output->appendPlainText("Осевой меридиан (В градусах): " + QString::number(passportR.mainLongi));
			ui.pp_output->appendPlainText("Параллель главной точки (В градусах): " + QString::number(passportR.latiMainPnt));
			ui.pp_output->appendPlainText("Флаг инвертирования изображения: " + QString::number(passportR.imgInv));
		}
	}
	else 
	{
		ui.pp_output->appendPlainText("Паспорт для этого формата не поддерживается.");
		return;
	}
	if (result == false)
		ui.pp_output->appendPlainText("Не удалось прочитать пасспорт");
	return;
}

void srv_utils_debugger::pp_showDimHead(DIM_HEAD *dh)
{
	if (dh == NULL)
		return;
	ui.pp_output->appendPlainText("ВЕРСИЯ: " + QString::number(dh->Version));
	ui.pp_output->appendPlainText("КОНТРОЛЬНАЯ СУММА: " + QString::number(dh->CheckSum));
	ui.pp_output->appendPlainText("КОЛ-ВО ОБЪЕКТОВ В ФАЙЛЕ: " + QString::number(dh->ObjectsCount));
	ui.pp_output->appendPlainText("N ПОСЛЕДНЕГО ОБЪЕКТА: " + QString::number(dh->LastObject));
	ui.pp_output->appendPlainText("МАСШТАБ ЛИСТА: " + QString::number(dh->Scale));
	ui.pp_output->appendPlainText("РАЗМЕР dig'а В mm: " + QString::number(dh->DigSize));
	ui.pp_output->appendPlainText("ПРОЕКЦИЯ ЛИСТА КАРТЫ: " + QString::number(dh->Projection));
	ui.pp_output->appendPlainText("ТИП ЛИСТА КАРТЫ: " + QString::number(dh->Type));
	ui.pp_output->appendPlainText("СИСТЕМА КООРДИНАТ: " + QString::number(dh->CoordSystem));
	ui.pp_output->appendPlainText("СИСТЕМА ВЫСОТ: " + QString::number(dh->HSystem));
	ui.pp_output->appendPlainText("РАМКА (КООРДИНАТЫ УГЛОВ) ЛИСТА В ГЕОДЕЗИЧЕСКИХ КООРДИНАТАХ BL:\nлн "
		+ QString::number(dh->GeoFrame[0].x, 'f', 12) + ", " + QString::number(dh->GeoFrame[0].y, 'f', 12) + "\n"
		+ "лв " + QString::number(dh->GeoFrame[1].x, 'f', 12) + ", " + QString::number(dh->GeoFrame[1].y, 'f', 12) + "\n"
		+ "пв " + QString::number(dh->GeoFrame[2].x, 'f', 12) + ", " + QString::number(dh->GeoFrame[2].y, 'f', 12) + "\n"
		+ "пн " + QString::number(dh->GeoFrame[3].x, 'f', 12) + ", " + QString::number(dh->GeoFrame[3].y, 'f', 12));
	ui.pp_output->appendPlainText("РАМКА ЛИСТА В ПЛАНШЕТНЫХ КООРДИНАТАХ XY:\nлн "
		+ QString::number(dh->DigFrame[0].x, 'f', 12) + ", " + QString::number(dh->DigFrame[0].y, 'f', 12) + "\n"
		+ "лв " + QString::number(dh->DigFrame[1].x, 'f', 12) + ", " + QString::number(dh->DigFrame[1].y, 'f', 12) + "\n"
		+ "пв " + QString::number(dh->DigFrame[2].x, 'f', 12) + ", " + QString::number(dh->DigFrame[2].y, 'f', 12) + "\n"
		+ "пн " + QString::number(dh->DigFrame[3].x, 'f', 12) + ", " + QString::number(dh->DigFrame[3].y, 'f', 12));
	ui.pp_output->appendPlainText("КООРДИНАТЫ XY ЛЕВОГО НИЖНЕГО УГЛА ПЛАНШЕТА ЛИСТА: " + QString::number(dh->DigMinXY.x) + ", " + QString::number(dh->DigMinXY.y));
	ui.pp_output->appendPlainText("КООРДИНАТЫ XY ПРАВОГО ВЕРХНЕГО УГЛА ПЛАНШЕТА ЛИСТА: " + QString::number(dh->DigMaxXY.x) + ", " + QString::number(dh->DigMaxXY.y));
	ui.pp_output->appendPlainText("НОМЕНКЛАТУРА ЛИСТА: " + QString(dh->Nom));
	ui.pp_output->appendPlainText("УСЛОВНОЕ НАЗВАНИЕ ЛИСТА: " + QString(dh->ListName));
	ui.pp_output->appendPlainText("ИНФОРМАЦИЯ ПОЛЬЗОВАТЕЛЯ: " + QString(dh->UserInfo));
	ui.pp_output->appendPlainText("1-Я ГЛАВНАЯ ПАРАЛЛЕЛЬ: " + QString::number(dh->Paral1));
	ui.pp_output->appendPlainText("2-Я ГЛАВНАЯ ПАРАЛЛЕЛЬ: " + QString::number(dh->Paral2));
	ui.pp_output->appendPlainText("ОСЕВОЙ МЕРИДИАН: " + QString::number(dh->AxisMerid));
	ui.pp_output->appendPlainText("ПАРАЛЛЕЛЬ ГЛАВНОЙ ТОЧКИ: " + QString::number(dh->ParalMain));
	ui.pp_output->appendPlainText("ВИД ЭЛЛИПОСИДА: " + QString::number(dh->Ellipsoid));
	ui.pp_output->appendPlainText("ДАТА СЪЕМКИ МЕСТНОСТИ: " + QString::number(dh->OrigDateY) + "/" + QString::number(dh->OrigDateM) + "/" + QString::number(dh->OrigDateD));
	ui.pp_output->appendPlainText("ВЫСОТА СЕЧЕНИЯ РЕЛЬЕФА: " + QString::number(dh->ReliefH));
	ui.pp_output->appendPlainText("МАГНИТНОЕ СКЛОНЕНИЕ: " + QString::number(dh->MagnitSklon));
	ui.pp_output->appendPlainText("СРЕДНЕЕ СБЛИЖЕНИЕ МЕРИДИАНОВ: " + QString::number(dh->MerSblij));
	ui.pp_output->appendPlainText("ГОДОВОЕ ИЗМЕНЕНИЕ МАГНИТНОГО СКЛОНЕНИЯ: " + QString::number(dh->MagnitSklonY));
	ui.pp_output->appendPlainText("ПРЯМОУГОЛЬНЫЕ КООРДИНАТЫ УГЛОВ ЛИСТА В ДЕЦИМЕТРАХ:\nлн "
		+ QString::number(dh->XY_Frame[0].x) + ", " + QString::number(dh->XY_Frame[0].y) + "\n"
		+ "лв " + QString::number(dh->XY_Frame[1].x) + ", " + QString::number(dh->XY_Frame[1].y) + "\n"
		+ "пв " + QString::number(dh->XY_Frame[2].x) + ", " + QString::number(dh->XY_Frame[2].y) + "\n"
		+ "пн " + QString::number(dh->XY_Frame[3].x) + ", " + QString::number(dh->XY_Frame[3].y));
	ui.pp_output->appendPlainText("ШИРОТА ПОЛЮСА: " + QString::number(dh->PolusB));
	ui.pp_output->appendPlainText("ДОЛГОТА ПОЛЮСА: " + QString::number(dh->PolusL));
}