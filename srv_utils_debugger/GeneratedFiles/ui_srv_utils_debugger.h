/********************************************************************************
** Form generated from reading UI file 'srv_utils_debugger.ui'
**
** Created: Mon 27. Jun 12:25:20 2016
**      by: Qt User Interface Compiler version 4.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SRV_UTILS_DEBUGGER_H
#define UI_SRV_UTILS_DEBUGGER_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QComboBox>
#include <QtGui/QGridLayout>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QListWidget>
#include <QtGui/QMainWindow>
#include <QtGui/QMenuBar>
#include <QtGui/QPlainTextEdit>
#include <QtGui/QProgressBar>
#include <QtGui/QPushButton>
#include <QtGui/QRadioButton>
#include <QtGui/QScrollArea>
#include <QtGui/QSpacerItem>
#include <QtGui/QSpinBox>
#include <QtGui/QStatusBar>
#include <QtGui/QTabWidget>
#include <QtGui/QToolBar>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_srv_utils_debuggerClass
{
public:
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout;
    QTabWidget *tabWidget;
    QWidget *tab;
    QGridLayout *gridLayout;
    QHBoxLayout *u_transformResult;
    QLabel *label_7;
    QLabel *label_8;
    QLineEdit *u_resultX;
    QLabel *label_9;
    QLineEdit *u_resultY;
    QGroupBox *u_transformProj4;
    QGridLayout *gridLayout_2;
    QPushButton *u_transformProj4_test;
    QHBoxLayout *horizontalLayout_6;
    QLabel *label_5;
    QLineEdit *u_transformProj4X;
    QLabel *label_6;
    QLineEdit *u_transformProj4Y;
    QHBoxLayout *horizontalLayout_5;
    QLabel *label_4;
    QLineEdit *u_transformProj4Dest;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label_3;
    QLineEdit *u_transformProj4Source;
    QGroupBox *u_transform;
    QGridLayout *gridLayout_4;
    QPushButton *u_transform_test;
    QHBoxLayout *horizontalLayout_10;
    QLabel *label_16;
    QLineEdit *u_transformX;
    QLabel *label_17;
    QLineEdit *u_transformY;
    QHBoxLayout *horizontalLayout_9;
    QLabel *label_14;
    QLineEdit *u_transformL0;
    QLabel *label_15;
    QComboBox *u_transformNorth;
    QHBoxLayout *horizontalLayout_8;
    QLabel *label_12;
    QLineEdit *u_transformB0;
    QLabel *label_13;
    QLineEdit *u_transformB1;
    QHBoxLayout *horizontalLayout_7;
    QLabel *label_11;
    QComboBox *u_transformProj;
    QLabel *label_10;
    QComboBox *u_transformEllips;
    QComboBox *u_transformSelect;
    QWidget *pmTab;
    QGridLayout *gridLayout_3;
    QHBoxLayout *horizontalLayout_2;
    QLabel *pm_output;
    QSpacerItem *horizontalSpacer;
    QHBoxLayout *horizontalLayout;
    QLabel *label_2;
    QSpinBox *pm_sizeSpin;
    QLabel *label;
    QSpinBox *pm_qualitySpin;
    QSpacerItem *horizontalSpacer_2;
    QPushButton *pm_save;
    QScrollArea *scrollArea;
    QWidget *scrollAreaWidgetContents;
    QVBoxLayout *verticalLayout_2;
    QLabel *pm_img;
    QPushButton *pm_testButton;
    QLineEdit *pm_path;
    QPushButton *pm_pathButton;
    QWidget *rmTab;
    QGridLayout *gridLayout_6;
    QHBoxLayout *horizontalLayout_3;
    QPushButton *rm_testButton;
    QProgressBar *rm_progress;
    QLabel *rm_count;
    QSpacerItem *horizontalSpacer_3;
    QPushButton *rm_addListButton;
    QPushButton *rm_removeListButton;
    QLabel *label_18;
    QLineEdit *rm_resultPath;
    QListWidget *rm_mapLists;
    QPushButton *rm_resultPathButton;
    QLineEdit *rm_clsPath;
    QLabel *label_19;
    QPushButton *rm_clsPathButton;
    QHBoxLayout *horizontalLayout_11;
    QLabel *label_20;
    QComboBox *rm_proj;
    QRadioButton *rm_auto;
    QRadioButton *rm_center;
    QRadioButton *rm_zone;
    QSpinBox *rm_zoneNumber;
    QWidget *ppTab;
    QGridLayout *gridLayout_5;
    QPushButton *pp_s57_gdal;
    QSpacerItem *verticalSpacer;
    QPushButton *pp_base;
    QPlainTextEdit *pp_output;
    QPushButton *pp_tiff;
    QPushButton *pp_gdal;
    QHBoxLayout *horizontalLayout_12;
    QLineEdit *pp_filePath;
    QPushButton *pp_pathButton;
    QPushButton *pp_rsw_mtw;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *srv_utils_debuggerClass)
    {
        if (srv_utils_debuggerClass->objectName().isEmpty())
            srv_utils_debuggerClass->setObjectName(QString::fromUtf8("srv_utils_debuggerClass"));
        srv_utils_debuggerClass->resize(645, 458);
        centralWidget = new QWidget(srv_utils_debuggerClass);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(centralWidget->sizePolicy().hasHeightForWidth());
        centralWidget->setSizePolicy(sizePolicy);
        verticalLayout = new QVBoxLayout(centralWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        tabWidget = new QTabWidget(centralWidget);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tabWidget->setTabShape(QTabWidget::Rounded);
        tabWidget->setDocumentMode(false);
        tab = new QWidget();
        tab->setObjectName(QString::fromUtf8("tab"));
        sizePolicy.setHeightForWidth(tab->sizePolicy().hasHeightForWidth());
        tab->setSizePolicy(sizePolicy);
        gridLayout = new QGridLayout(tab);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        u_transformResult = new QHBoxLayout();
        u_transformResult->setSpacing(6);
        u_transformResult->setObjectName(QString::fromUtf8("u_transformResult"));
        label_7 = new QLabel(tab);
        label_7->setObjectName(QString::fromUtf8("label_7"));

        u_transformResult->addWidget(label_7);

        label_8 = new QLabel(tab);
        label_8->setObjectName(QString::fromUtf8("label_8"));

        u_transformResult->addWidget(label_8);

        u_resultX = new QLineEdit(tab);
        u_resultX->setObjectName(QString::fromUtf8("u_resultX"));

        u_transformResult->addWidget(u_resultX);

        label_9 = new QLabel(tab);
        label_9->setObjectName(QString::fromUtf8("label_9"));

        u_transformResult->addWidget(label_9);

        u_resultY = new QLineEdit(tab);
        u_resultY->setObjectName(QString::fromUtf8("u_resultY"));

        u_transformResult->addWidget(u_resultY);


        gridLayout->addLayout(u_transformResult, 2, 1, 1, 1);

        u_transformProj4 = new QGroupBox(tab);
        u_transformProj4->setObjectName(QString::fromUtf8("u_transformProj4"));
        gridLayout_2 = new QGridLayout(u_transformProj4);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        u_transformProj4_test = new QPushButton(u_transformProj4);
        u_transformProj4_test->setObjectName(QString::fromUtf8("u_transformProj4_test"));

        gridLayout_2->addWidget(u_transformProj4_test, 3, 1, 1, 1);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setSpacing(6);
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        label_5 = new QLabel(u_transformProj4);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        horizontalLayout_6->addWidget(label_5);

        u_transformProj4X = new QLineEdit(u_transformProj4);
        u_transformProj4X->setObjectName(QString::fromUtf8("u_transformProj4X"));

        horizontalLayout_6->addWidget(u_transformProj4X);

        label_6 = new QLabel(u_transformProj4);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        horizontalLayout_6->addWidget(label_6);

        u_transformProj4Y = new QLineEdit(u_transformProj4);
        u_transformProj4Y->setObjectName(QString::fromUtf8("u_transformProj4Y"));

        horizontalLayout_6->addWidget(u_transformProj4Y);


        gridLayout_2->addLayout(horizontalLayout_6, 2, 0, 1, 2);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setSpacing(6);
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        label_4 = new QLabel(u_transformProj4);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        horizontalLayout_5->addWidget(label_4);

        u_transformProj4Dest = new QLineEdit(u_transformProj4);
        u_transformProj4Dest->setObjectName(QString::fromUtf8("u_transformProj4Dest"));

        horizontalLayout_5->addWidget(u_transformProj4Dest);


        gridLayout_2->addLayout(horizontalLayout_5, 1, 0, 1, 2);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        label_3 = new QLabel(u_transformProj4);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        horizontalLayout_4->addWidget(label_3);

        u_transformProj4Source = new QLineEdit(u_transformProj4);
        u_transformProj4Source->setObjectName(QString::fromUtf8("u_transformProj4Source"));

        horizontalLayout_4->addWidget(u_transformProj4Source);


        gridLayout_2->addLayout(horizontalLayout_4, 0, 0, 1, 2);


        gridLayout->addWidget(u_transformProj4, 1, 0, 1, 2);

        u_transform = new QGroupBox(tab);
        u_transform->setObjectName(QString::fromUtf8("u_transform"));
        gridLayout_4 = new QGridLayout(u_transform);
        gridLayout_4->setSpacing(6);
        gridLayout_4->setContentsMargins(11, 11, 11, 11);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        u_transform_test = new QPushButton(u_transform);
        u_transform_test->setObjectName(QString::fromUtf8("u_transform_test"));

        gridLayout_4->addWidget(u_transform_test, 4, 1, 1, 1);

        horizontalLayout_10 = new QHBoxLayout();
        horizontalLayout_10->setSpacing(6);
        horizontalLayout_10->setObjectName(QString::fromUtf8("horizontalLayout_10"));
        label_16 = new QLabel(u_transform);
        label_16->setObjectName(QString::fromUtf8("label_16"));

        horizontalLayout_10->addWidget(label_16);

        u_transformX = new QLineEdit(u_transform);
        u_transformX->setObjectName(QString::fromUtf8("u_transformX"));

        horizontalLayout_10->addWidget(u_transformX);

        label_17 = new QLabel(u_transform);
        label_17->setObjectName(QString::fromUtf8("label_17"));

        horizontalLayout_10->addWidget(label_17);

        u_transformY = new QLineEdit(u_transform);
        u_transformY->setObjectName(QString::fromUtf8("u_transformY"));

        horizontalLayout_10->addWidget(u_transformY);


        gridLayout_4->addLayout(horizontalLayout_10, 3, 0, 1, 2);

        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setSpacing(6);
        horizontalLayout_9->setObjectName(QString::fromUtf8("horizontalLayout_9"));
        label_14 = new QLabel(u_transform);
        label_14->setObjectName(QString::fromUtf8("label_14"));

        horizontalLayout_9->addWidget(label_14);

        u_transformL0 = new QLineEdit(u_transform);
        u_transformL0->setObjectName(QString::fromUtf8("u_transformL0"));

        horizontalLayout_9->addWidget(u_transformL0);

        label_15 = new QLabel(u_transform);
        label_15->setObjectName(QString::fromUtf8("label_15"));

        horizontalLayout_9->addWidget(label_15);

        u_transformNorth = new QComboBox(u_transform);
        u_transformNorth->setObjectName(QString::fromUtf8("u_transformNorth"));

        horizontalLayout_9->addWidget(u_transformNorth);


        gridLayout_4->addLayout(horizontalLayout_9, 2, 0, 1, 2);

        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setSpacing(6);
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        label_12 = new QLabel(u_transform);
        label_12->setObjectName(QString::fromUtf8("label_12"));

        horizontalLayout_8->addWidget(label_12);

        u_transformB0 = new QLineEdit(u_transform);
        u_transformB0->setObjectName(QString::fromUtf8("u_transformB0"));

        horizontalLayout_8->addWidget(u_transformB0);

        label_13 = new QLabel(u_transform);
        label_13->setObjectName(QString::fromUtf8("label_13"));

        horizontalLayout_8->addWidget(label_13);

        u_transformB1 = new QLineEdit(u_transform);
        u_transformB1->setObjectName(QString::fromUtf8("u_transformB1"));

        horizontalLayout_8->addWidget(u_transformB1);


        gridLayout_4->addLayout(horizontalLayout_8, 1, 0, 1, 2);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setSpacing(6);
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        label_11 = new QLabel(u_transform);
        label_11->setObjectName(QString::fromUtf8("label_11"));

        horizontalLayout_7->addWidget(label_11);

        u_transformProj = new QComboBox(u_transform);
        u_transformProj->setObjectName(QString::fromUtf8("u_transformProj"));

        horizontalLayout_7->addWidget(u_transformProj);

        label_10 = new QLabel(u_transform);
        label_10->setObjectName(QString::fromUtf8("label_10"));

        horizontalLayout_7->addWidget(label_10);

        u_transformEllips = new QComboBox(u_transform);
        u_transformEllips->setObjectName(QString::fromUtf8("u_transformEllips"));

        horizontalLayout_7->addWidget(u_transformEllips);


        gridLayout_4->addLayout(horizontalLayout_7, 0, 0, 1, 2);

        u_transformSelect = new QComboBox(u_transform);
        u_transformSelect->setObjectName(QString::fromUtf8("u_transformSelect"));

        gridLayout_4->addWidget(u_transformSelect, 4, 0, 1, 1);


        gridLayout->addWidget(u_transform, 0, 0, 1, 2);

        tabWidget->addTab(tab, QString());
        pmTab = new QWidget();
        pmTab->setObjectName(QString::fromUtf8("pmTab"));
        gridLayout_3 = new QGridLayout(pmTab);
        gridLayout_3->setSpacing(6);
        gridLayout_3->setContentsMargins(11, 11, 11, 11);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        pm_output = new QLabel(pmTab);
        pm_output->setObjectName(QString::fromUtf8("pm_output"));

        horizontalLayout_2->addWidget(pm_output);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer);


        gridLayout_3->addLayout(horizontalLayout_2, 4, 0, 1, 4);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label_2 = new QLabel(pmTab);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        horizontalLayout->addWidget(label_2);

        pm_sizeSpin = new QSpinBox(pmTab);
        pm_sizeSpin->setObjectName(QString::fromUtf8("pm_sizeSpin"));
        pm_sizeSpin->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        pm_sizeSpin->setMinimum(64);
        pm_sizeSpin->setMaximum(4096);
        pm_sizeSpin->setSingleStep(16);
        pm_sizeSpin->setValue(256);

        horizontalLayout->addWidget(pm_sizeSpin);

        label = new QLabel(pmTab);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout->addWidget(label);

        pm_qualitySpin = new QSpinBox(pmTab);
        pm_qualitySpin->setObjectName(QString::fromUtf8("pm_qualitySpin"));
        pm_qualitySpin->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        pm_qualitySpin->setMaximum(2);
        pm_qualitySpin->setValue(2);

        horizontalLayout->addWidget(pm_qualitySpin);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);

        pm_save = new QPushButton(pmTab);
        pm_save->setObjectName(QString::fromUtf8("pm_save"));
        pm_save->setEnabled(false);

        horizontalLayout->addWidget(pm_save);


        gridLayout_3->addLayout(horizontalLayout, 1, 0, 1, 4);

        scrollArea = new QScrollArea(pmTab);
        scrollArea->setObjectName(QString::fromUtf8("scrollArea"));
        scrollArea->setFrameShape(QFrame::Box);
        scrollArea->setFrameShadow(QFrame::Plain);
        scrollArea->setWidgetResizable(true);
        scrollArea->setAlignment(Qt::AlignCenter);
        scrollAreaWidgetContents = new QWidget();
        scrollAreaWidgetContents->setObjectName(QString::fromUtf8("scrollAreaWidgetContents"));
        scrollAreaWidgetContents->setGeometry(QRect(0, 0, 601, 253));
        sizePolicy.setHeightForWidth(scrollAreaWidgetContents->sizePolicy().hasHeightForWidth());
        scrollAreaWidgetContents->setSizePolicy(sizePolicy);
        verticalLayout_2 = new QVBoxLayout(scrollAreaWidgetContents);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        pm_img = new QLabel(scrollAreaWidgetContents);
        pm_img->setObjectName(QString::fromUtf8("pm_img"));
        sizePolicy.setHeightForWidth(pm_img->sizePolicy().hasHeightForWidth());
        pm_img->setSizePolicy(sizePolicy);
        pm_img->setFrameShape(QFrame::NoFrame);
        pm_img->setFrameShadow(QFrame::Plain);
        pm_img->setLineWidth(1);
        pm_img->setAlignment(Qt::AlignCenter);

        verticalLayout_2->addWidget(pm_img);

        scrollArea->setWidget(scrollAreaWidgetContents);

        gridLayout_3->addWidget(scrollArea, 2, 0, 1, 4);

        pm_testButton = new QPushButton(pmTab);
        pm_testButton->setObjectName(QString::fromUtf8("pm_testButton"));

        gridLayout_3->addWidget(pm_testButton, 0, 3, 1, 1);

        pm_path = new QLineEdit(pmTab);
        pm_path->setObjectName(QString::fromUtf8("pm_path"));

        gridLayout_3->addWidget(pm_path, 0, 0, 1, 2);

        pm_pathButton = new QPushButton(pmTab);
        pm_pathButton->setObjectName(QString::fromUtf8("pm_pathButton"));
        pm_pathButton->setMaximumSize(QSize(20, 16777215));

        gridLayout_3->addWidget(pm_pathButton, 0, 2, 1, 1);

        tabWidget->addTab(pmTab, QString());
        rmTab = new QWidget();
        rmTab->setObjectName(QString::fromUtf8("rmTab"));
        rmTab->setEnabled(true);
        gridLayout_6 = new QGridLayout(rmTab);
        gridLayout_6->setSpacing(6);
        gridLayout_6->setContentsMargins(11, 11, 11, 11);
        gridLayout_6->setObjectName(QString::fromUtf8("gridLayout_6"));
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        rm_testButton = new QPushButton(rmTab);
        rm_testButton->setObjectName(QString::fromUtf8("rm_testButton"));
        rm_testButton->setEnabled(false);

        horizontalLayout_3->addWidget(rm_testButton);

        rm_progress = new QProgressBar(rmTab);
        rm_progress->setObjectName(QString::fromUtf8("rm_progress"));
        rm_progress->setEnabled(false);
        rm_progress->setMaximumSize(QSize(100, 16777215));
        rm_progress->setValue(0);
        rm_progress->setTextVisible(false);

        horizontalLayout_3->addWidget(rm_progress);

        rm_count = new QLabel(rmTab);
        rm_count->setObjectName(QString::fromUtf8("rm_count"));

        horizontalLayout_3->addWidget(rm_count);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_3);

        rm_addListButton = new QPushButton(rmTab);
        rm_addListButton->setObjectName(QString::fromUtf8("rm_addListButton"));
        rm_addListButton->setMaximumSize(QSize(25, 16777215));

        horizontalLayout_3->addWidget(rm_addListButton);

        rm_removeListButton = new QPushButton(rmTab);
        rm_removeListButton->setObjectName(QString::fromUtf8("rm_removeListButton"));
        rm_removeListButton->setEnabled(false);
        rm_removeListButton->setMaximumSize(QSize(25, 16777215));

        horizontalLayout_3->addWidget(rm_removeListButton);


        gridLayout_6->addLayout(horizontalLayout_3, 1, 0, 1, 3);

        label_18 = new QLabel(rmTab);
        label_18->setObjectName(QString::fromUtf8("label_18"));

        gridLayout_6->addWidget(label_18, 6, 0, 1, 1);

        rm_resultPath = new QLineEdit(rmTab);
        rm_resultPath->setObjectName(QString::fromUtf8("rm_resultPath"));

        gridLayout_6->addWidget(rm_resultPath, 6, 1, 1, 1);

        rm_mapLists = new QListWidget(rmTab);
        rm_mapLists->setObjectName(QString::fromUtf8("rm_mapLists"));
        rm_mapLists->setEnabled(true);

        gridLayout_6->addWidget(rm_mapLists, 0, 0, 1, 3);

        rm_resultPathButton = new QPushButton(rmTab);
        rm_resultPathButton->setObjectName(QString::fromUtf8("rm_resultPathButton"));
        rm_resultPathButton->setMaximumSize(QSize(20, 16777215));

        gridLayout_6->addWidget(rm_resultPathButton, 6, 2, 1, 1);

        rm_clsPath = new QLineEdit(rmTab);
        rm_clsPath->setObjectName(QString::fromUtf8("rm_clsPath"));

        gridLayout_6->addWidget(rm_clsPath, 5, 1, 1, 1);

        label_19 = new QLabel(rmTab);
        label_19->setObjectName(QString::fromUtf8("label_19"));

        gridLayout_6->addWidget(label_19, 5, 0, 1, 1);

        rm_clsPathButton = new QPushButton(rmTab);
        rm_clsPathButton->setObjectName(QString::fromUtf8("rm_clsPathButton"));
        rm_clsPathButton->setMaximumSize(QSize(20, 16777215));

        gridLayout_6->addWidget(rm_clsPathButton, 5, 2, 1, 1);

        horizontalLayout_11 = new QHBoxLayout();
        horizontalLayout_11->setSpacing(6);
        horizontalLayout_11->setObjectName(QString::fromUtf8("horizontalLayout_11"));
        label_20 = new QLabel(rmTab);
        label_20->setObjectName(QString::fromUtf8("label_20"));
        label_20->setMaximumSize(QSize(90, 16777215));

        horizontalLayout_11->addWidget(label_20);

        rm_proj = new QComboBox(rmTab);
        rm_proj->setObjectName(QString::fromUtf8("rm_proj"));

        horizontalLayout_11->addWidget(rm_proj);

        rm_auto = new QRadioButton(rmTab);
        rm_auto->setObjectName(QString::fromUtf8("rm_auto"));
        rm_auto->setMaximumSize(QSize(50, 16777215));
        rm_auto->setChecked(true);

        horizontalLayout_11->addWidget(rm_auto);

        rm_center = new QRadioButton(rmTab);
        rm_center->setObjectName(QString::fromUtf8("rm_center"));
        rm_center->setMaximumSize(QSize(50, 16777215));

        horizontalLayout_11->addWidget(rm_center);

        rm_zone = new QRadioButton(rmTab);
        rm_zone->setObjectName(QString::fromUtf8("rm_zone"));
        rm_zone->setMaximumSize(QSize(50, 16777215));

        horizontalLayout_11->addWidget(rm_zone);

        rm_zoneNumber = new QSpinBox(rmTab);
        rm_zoneNumber->setObjectName(QString::fromUtf8("rm_zoneNumber"));
        rm_zoneNumber->setEnabled(false);
        rm_zoneNumber->setMaximumSize(QSize(50, 16777215));
        rm_zoneNumber->setMaximum(80);

        horizontalLayout_11->addWidget(rm_zoneNumber);


        gridLayout_6->addLayout(horizontalLayout_11, 2, 0, 1, 3);

        tabWidget->addTab(rmTab, QString());
        ppTab = new QWidget();
        ppTab->setObjectName(QString::fromUtf8("ppTab"));
        gridLayout_5 = new QGridLayout(ppTab);
        gridLayout_5->setSpacing(6);
        gridLayout_5->setContentsMargins(11, 11, 11, 11);
        gridLayout_5->setObjectName(QString::fromUtf8("gridLayout_5"));
        pp_s57_gdal = new QPushButton(ppTab);
        pp_s57_gdal->setObjectName(QString::fromUtf8("pp_s57_gdal"));

        gridLayout_5->addWidget(pp_s57_gdal, 4, 1, 1, 1);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_5->addItem(verticalSpacer, 7, 1, 4, 1);

        pp_base = new QPushButton(ppTab);
        pp_base->setObjectName(QString::fromUtf8("pp_base"));

        gridLayout_5->addWidget(pp_base, 2, 1, 1, 1);

        pp_output = new QPlainTextEdit(ppTab);
        pp_output->setObjectName(QString::fromUtf8("pp_output"));

        gridLayout_5->addWidget(pp_output, 2, 0, 9, 1);

        pp_tiff = new QPushButton(ppTab);
        pp_tiff->setObjectName(QString::fromUtf8("pp_tiff"));

        gridLayout_5->addWidget(pp_tiff, 5, 1, 1, 1);

        pp_gdal = new QPushButton(ppTab);
        pp_gdal->setObjectName(QString::fromUtf8("pp_gdal"));

        gridLayout_5->addWidget(pp_gdal, 3, 1, 1, 1);

        horizontalLayout_12 = new QHBoxLayout();
        horizontalLayout_12->setSpacing(6);
        horizontalLayout_12->setObjectName(QString::fromUtf8("horizontalLayout_12"));
        pp_filePath = new QLineEdit(ppTab);
        pp_filePath->setObjectName(QString::fromUtf8("pp_filePath"));

        horizontalLayout_12->addWidget(pp_filePath);

        pp_pathButton = new QPushButton(ppTab);
        pp_pathButton->setObjectName(QString::fromUtf8("pp_pathButton"));
        pp_pathButton->setMaximumSize(QSize(20, 16777215));

        horizontalLayout_12->addWidget(pp_pathButton);


        gridLayout_5->addLayout(horizontalLayout_12, 0, 0, 1, 2);

        pp_rsw_mtw = new QPushButton(ppTab);
        pp_rsw_mtw->setObjectName(QString::fromUtf8("pp_rsw_mtw"));

        gridLayout_5->addWidget(pp_rsw_mtw, 6, 1, 1, 1);

        tabWidget->addTab(ppTab, QString());

        verticalLayout->addWidget(tabWidget);

        srv_utils_debuggerClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(srv_utils_debuggerClass);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 645, 21));
        srv_utils_debuggerClass->setMenuBar(menuBar);
        mainToolBar = new QToolBar(srv_utils_debuggerClass);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        srv_utils_debuggerClass->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(srv_utils_debuggerClass);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        srv_utils_debuggerClass->setStatusBar(statusBar);
        QWidget::setTabOrder(tabWidget, u_transformProj);
        QWidget::setTabOrder(u_transformProj, u_transformEllips);
        QWidget::setTabOrder(u_transformEllips, u_transformB0);
        QWidget::setTabOrder(u_transformB0, u_transformB1);
        QWidget::setTabOrder(u_transformB1, u_transformL0);
        QWidget::setTabOrder(u_transformL0, u_transformNorth);
        QWidget::setTabOrder(u_transformNorth, u_transformX);
        QWidget::setTabOrder(u_transformX, u_transformY);
        QWidget::setTabOrder(u_transformY, u_transformSelect);
        QWidget::setTabOrder(u_transformSelect, u_transform_test);
        QWidget::setTabOrder(u_transform_test, u_transformProj4Source);
        QWidget::setTabOrder(u_transformProj4Source, u_transformProj4Dest);
        QWidget::setTabOrder(u_transformProj4Dest, u_transformProj4X);
        QWidget::setTabOrder(u_transformProj4X, u_transformProj4Y);
        QWidget::setTabOrder(u_transformProj4Y, u_transformProj4_test);
        QWidget::setTabOrder(u_transformProj4_test, u_resultX);
        QWidget::setTabOrder(u_resultX, u_resultY);
        QWidget::setTabOrder(u_resultY, pm_path);
        QWidget::setTabOrder(pm_path, pm_pathButton);
        QWidget::setTabOrder(pm_pathButton, pm_testButton);
        QWidget::setTabOrder(pm_testButton, pm_sizeSpin);
        QWidget::setTabOrder(pm_sizeSpin, pm_qualitySpin);
        QWidget::setTabOrder(pm_qualitySpin, scrollArea);

        retranslateUi(srv_utils_debuggerClass);

        tabWidget->setCurrentIndex(3);


        QMetaObject::connectSlotsByName(srv_utils_debuggerClass);
    } // setupUi

    void retranslateUi(QMainWindow *srv_utils_debuggerClass)
    {
        srv_utils_debuggerClass->setWindowTitle(QApplication::translate("srv_utils_debuggerClass", "srv_utils_debugger", 0, QApplication::UnicodeUTF8));
        label_7->setText(QApplication::translate("srv_utils_debuggerClass", "\320\240\320\265\320\267\321\203\320\273\321\214\321\202\320\260\321\202:", 0, QApplication::UnicodeUTF8));
        label_8->setText(QApplication::translate("srv_utils_debuggerClass", "X:", 0, QApplication::UnicodeUTF8));
        u_resultX->setText(QApplication::translate("srv_utils_debuggerClass", "0", 0, QApplication::UnicodeUTF8));
        label_9->setText(QApplication::translate("srv_utils_debuggerClass", "Y:", 0, QApplication::UnicodeUTF8));
        u_resultY->setText(QApplication::translate("srv_utils_debuggerClass", "0", 0, QApplication::UnicodeUTF8));
        u_transformProj4->setTitle(QApplication::translate("srv_utils_debuggerClass", "Transform Proj4", 0, QApplication::UnicodeUTF8));
        u_transformProj4_test->setText(QApplication::translate("srv_utils_debuggerClass", "\320\237\320\276\321\201\321\207\320\270\321\202\320\260\321\202\321\214", 0, QApplication::UnicodeUTF8));
        label_5->setText(QApplication::translate("srv_utils_debuggerClass", "X:", 0, QApplication::UnicodeUTF8));
        u_transformProj4X->setText(QApplication::translate("srv_utils_debuggerClass", "0", 0, QApplication::UnicodeUTF8));
        label_6->setText(QApplication::translate("srv_utils_debuggerClass", "Y:", 0, QApplication::UnicodeUTF8));
        u_transformProj4Y->setText(QApplication::translate("srv_utils_debuggerClass", "0", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("srv_utils_debuggerClass", "Proj4 \321\201\321\202\321\200\320\276\320\272\320\260 \320\275\320\260\320\267\320\275\320\260\321\207\320\265\320\275\320\270\321\217:", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("srv_utils_debuggerClass", "Proj4 \321\201\321\202\321\200\320\276\320\272\320\260 \320\270\321\201\321\205\320\276\320\264\320\275\321\213\321\205 \320\264\320\260\320\275\320\275\321\213\321\205:", 0, QApplication::UnicodeUTF8));
        u_transform->setTitle(QApplication::translate("srv_utils_debuggerClass", "Transform", 0, QApplication::UnicodeUTF8));
        u_transform_test->setText(QApplication::translate("srv_utils_debuggerClass", "\320\237\320\276\321\201\321\207\320\270\321\202\320\260\321\202\321\214", 0, QApplication::UnicodeUTF8));
        label_16->setText(QApplication::translate("srv_utils_debuggerClass", "X:", 0, QApplication::UnicodeUTF8));
        u_transformX->setText(QApplication::translate("srv_utils_debuggerClass", "0", 0, QApplication::UnicodeUTF8));
        label_17->setText(QApplication::translate("srv_utils_debuggerClass", "Y:", 0, QApplication::UnicodeUTF8));
        u_transformY->setText(QApplication::translate("srv_utils_debuggerClass", "0", 0, QApplication::UnicodeUTF8));
        label_14->setText(QApplication::translate("srv_utils_debuggerClass", "\320\236\321\201\320\265\320\262\320\276\320\271 \320\274\320\265\321\200\320\270\320\264\320\270\320\260\320\275:", 0, QApplication::UnicodeUTF8));
        u_transformL0->setText(QApplication::translate("srv_utils_debuggerClass", "0", 0, QApplication::UnicodeUTF8));
        label_15->setText(QApplication::translate("srv_utils_debuggerClass", "\320\241\320\265\320\262\320\265\321\200\320\275\320\260\321\217 \320\277\320\276\320\273\321\203\321\201\321\204\320\265\321\200\320\260:", 0, QApplication::UnicodeUTF8));
        u_transformNorth->clear();
        u_transformNorth->insertItems(0, QStringList()
         << QApplication::translate("srv_utils_debuggerClass", "\320\235\320\265\321\202", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("srv_utils_debuggerClass", "\320\224\320\260", 0, QApplication::UnicodeUTF8)
        );
        label_12->setText(QApplication::translate("srv_utils_debuggerClass", "1-\320\260\321\217 \320\263\320\273\320\260\320\262\320\275\320\260\321\217 \320\277\320\260\321\200\320\260\320\273\320\273\320\265\320\273\321\214:", 0, QApplication::UnicodeUTF8));
        u_transformB0->setText(QApplication::translate("srv_utils_debuggerClass", "0", 0, QApplication::UnicodeUTF8));
        label_13->setText(QApplication::translate("srv_utils_debuggerClass", "2-\320\260\321\217 \320\263\320\273\320\260\320\262\320\275\320\260\321\217 \320\277\320\260\321\200\320\260\320\273\320\273\320\265\320\273\321\214:", 0, QApplication::UnicodeUTF8));
        u_transformB1->setText(QApplication::translate("srv_utils_debuggerClass", "0", 0, QApplication::UnicodeUTF8));
        label_11->setText(QApplication::translate("srv_utils_debuggerClass", "\320\237\321\200\320\276\320\265\320\272\321\206\320\270\321\217:", 0, QApplication::UnicodeUTF8));
        label_10->setText(QApplication::translate("srv_utils_debuggerClass", "\320\255\320\273\320\273\320\270\320\277\321\201\320\276\320\270\320\264:", 0, QApplication::UnicodeUTF8));
        u_transformSelect->clear();
        u_transformSelect->insertItems(0, QStringList()
         << QApplication::translate("srv_utils_debuggerClass", "\320\274\320\265\321\202\321\200\321\213 \320\262 \320\263\321\200\320\260\320\264\321\203\321\201\321\213", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("srv_utils_debuggerClass", "\320\263\321\200\320\260\320\264\321\203\321\201\321\213 \320\262 \320\274\320\265\321\202\321\200\321\213", 0, QApplication::UnicodeUTF8)
        );
        tabWidget->setTabText(tabWidget->indexOf(tab), QApplication::translate("srv_utils_debuggerClass", "Utils", 0, QApplication::UnicodeUTF8));
        pm_output->setText(QString());
        label_2->setText(QApplication::translate("srv_utils_debuggerClass", "\320\240\320\260\320\267\320\274\320\265\321\200:", 0, QApplication::UnicodeUTF8));
        pm_sizeSpin->setSuffix(QApplication::translate("srv_utils_debuggerClass", " px", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("srv_utils_debuggerClass", "\320\241\320\263\320\273\320\260\320\266\320\270\320\262\320\260\320\275\320\270\320\265:", 0, QApplication::UnicodeUTF8));
        pm_save->setText(QApplication::translate("srv_utils_debuggerClass", "\320\241\320\276\321\205\321\200\320\260\320\275\320\270\321\202\321\214", 0, QApplication::UnicodeUTF8));
        pm_img->setText(QString());
        pm_testButton->setText(QApplication::translate("srv_utils_debuggerClass", "\320\242\320\265\321\201\321\202", 0, QApplication::UnicodeUTF8));
        pm_pathButton->setText(QApplication::translate("srv_utils_debuggerClass", "...", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabText(tabWidget->indexOf(pmTab), QApplication::translate("srv_utils_debuggerClass", "PreviewMaker", 0, QApplication::UnicodeUTF8));
        rm_testButton->setText(QApplication::translate("srv_utils_debuggerClass", "\320\241\321\210\320\270\321\202\321\214", 0, QApplication::UnicodeUTF8));
        rm_count->setText(QString());
        rm_addListButton->setText(QApplication::translate("srv_utils_debuggerClass", "+", 0, QApplication::UnicodeUTF8));
        rm_removeListButton->setText(QApplication::translate("srv_utils_debuggerClass", "-", 0, QApplication::UnicodeUTF8));
        label_18->setText(QApplication::translate("srv_utils_debuggerClass", "\320\240\320\265\320\267\321\203\320\273\321\214\321\202\320\260\321\202:", 0, QApplication::UnicodeUTF8));
        rm_resultPathButton->setText(QApplication::translate("srv_utils_debuggerClass", "...", 0, QApplication::UnicodeUTF8));
        label_19->setText(QApplication::translate("srv_utils_debuggerClass", "\320\232\320\273\320\260\321\201\321\201\320\270\321\204\320\270\320\272\321\202\320\276\321\200:", 0, QApplication::UnicodeUTF8));
        rm_clsPathButton->setText(QApplication::translate("srv_utils_debuggerClass", "...", 0, QApplication::UnicodeUTF8));
        label_20->setText(QApplication::translate("srv_utils_debuggerClass", "\320\237\321\200\320\276\320\265\320\272\321\206\320\270\321\217 \320\270 \320\267\320\276\320\275\320\260:", 0, QApplication::UnicodeUTF8));
        rm_proj->clear();
        rm_proj->insertItems(0, QStringList()
         << QApplication::translate("srv_utils_debuggerClass", "\321\200\320\260\320\262\320\275\320\276\321\203\320\263\320\276\320\273\321\214\320\275\320\260\321\217 \320\223\320\260\321\203\321\201\321\201\320\260-\320\232\321\200\321\216\320\263\320\265\321\200\320\260", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("srv_utils_debuggerClass", "\321\206\320\270\320\273\320\270\320\275\320\264\321\200\320\270\321\207\320\265\321\201\320\272\320\260\321\217 \320\277\321\200\321\217\320\274\320\260\321\217 \321\200\320\260\320\262\320\275\320\276\321\203\320\263\320\276\320\273\321\214\320\275\320\260\321\217 (\320\234\320\265\321\200\320\272\320\260\321\202\320\276\321\200\320\260)", 0, QApplication::UnicodeUTF8)
        );
        rm_auto->setText(QApplication::translate("srv_utils_debuggerClass", "\320\220\320\262\321\202\320\276", 0, QApplication::UnicodeUTF8));
        rm_center->setText(QApplication::translate("srv_utils_debuggerClass", "\320\246\320\265\320\275\321\202\321\200", 0, QApplication::UnicodeUTF8));
        rm_zone->setText(QApplication::translate("srv_utils_debuggerClass", "\320\227\320\276\320\275\320\260", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabText(tabWidget->indexOf(rmTab), QApplication::translate("srv_utils_debuggerClass", "RegionMaker", 0, QApplication::UnicodeUTF8));
        pp_s57_gdal->setText(QApplication::translate("srv_utils_debuggerClass", "S57_GDAL", 0, QApplication::UnicodeUTF8));
        pp_base->setText(QApplication::translate("srv_utils_debuggerClass", "BASE", 0, QApplication::UnicodeUTF8));
        pp_tiff->setText(QApplication::translate("srv_utils_debuggerClass", "GEOTIFF", 0, QApplication::UnicodeUTF8));
        pp_gdal->setText(QApplication::translate("srv_utils_debuggerClass", "GDAL", 0, QApplication::UnicodeUTF8));
        pp_pathButton->setText(QApplication::translate("srv_utils_debuggerClass", "...", 0, QApplication::UnicodeUTF8));
        pp_rsw_mtw->setText(QApplication::translate("srv_utils_debuggerClass", "RSW/MTW", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabText(tabWidget->indexOf(ppTab), QApplication::translate("srv_utils_debuggerClass", "Passports", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class srv_utils_debuggerClass: public Ui_srv_utils_debuggerClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SRV_UTILS_DEBUGGER_H
