/****************************************************************************
** Resource object code
**
** Created: Mon 27. Jun 12:25:21 2016
**      by: The Resource Compiler for Qt version 4.8.0
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <QtCore/qglobal.h>

QT_BEGIN_NAMESPACE

QT_END_NAMESPACE


int QT_MANGLE_NAMESPACE(qInitResources_srv_utils_debugger)()
{
    return 1;
}

Q_CONSTRUCTOR_FUNCTION(QT_MANGLE_NAMESPACE(qInitResources_srv_utils_debugger))

int QT_MANGLE_NAMESPACE(qCleanupResources_srv_utils_debugger)()
{
    return 1;
}

Q_DESTRUCTOR_FUNCTION(QT_MANGLE_NAMESPACE(qCleanupResources_srv_utils_debugger))

